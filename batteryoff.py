#!/usr/bin/python

from pymodbus.client.sync import ModbusTcpClient

inverter = ModbusTcpClient(host='192.168.1.200', port=502);#, baudrate=9600, stopbits=1, parity='N', bytesize=8, timeout=1)
inverter.connect()
rr = inverter.write_register(0x00, 2014) # Enter password
if rr != None:
    rr = inverter.write_register(0x1F, 2) # Manual control mode
    if rr != None:
        rr = inverter.write_register(0x52, 0) # Set output power to 0
        rr = inverter.write_register(0x51, 0) # Set charge power to 0
inverter.close()
